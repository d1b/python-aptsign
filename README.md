# APT ED25519 Repository signing reference implementation

Implementation of APT ED25519 signatures in Python.

## Usage

`aptsign generate-primary FILENAME`
: Generate a new primary key in the given file

`aptsign generate-subkey PRIMARY SUBKEY`
: Generate a new subkey in the file SUBKEY based on the primary key in file PRIMARY

`aptsign sign-file KEY`
: Sign standard input using the given key (primary or subkey), returns signed version on standard output

`aptsign verify-file`
: Verify the standard input, returns signed content on standard output

`aptsign public-key PRIVATEKEY`
: Print the public key for the private key in the given file to stdout

## Example

```
$ cat > Test  << EOF
> Key: value
> OtherKey: otherValue
>
> EOF
$ python3 -m aptsign generate-primary primary.sec
$ python3 -m aptsign generate-subkey primary.sec subkey.sec
$ cat primary.sec
private-key: nzXRBQ+S+IKUWDqLRjrnk9mqPCBMxLKzDoH4wS5RiBo=

$ cat subkey.sec
private-key: SnX+QPB327w8xUH+/bm8el5E/UM6rmizgwWNuxzyZGU=
subkey:  s1yq1YBOsjZYT9+8Hq2y2MBlvJ31EDjFf8OzgufpeFVDuO3aZMWDJJvU9SP5KHW+G+Uo25myfQRjQdojh8KqDf//////////AAAAABt1KuFrxNctO84dWeXgwiQutPJjUs2mA8uNqBcNGC7yPJs2x/ZVZ6DFIRRfzq1iq8EwzKA/sW/nIBSc3ri71Qw=

$ python3 -m aptsign sign-file primary.sec < Test
Key: value
OtherKey: otherValue
Signatures:
 s1yq1YBOsjZYT9+8Hq2y2MBlvJ31EDjFf8OzgufpeFV9wOKLg9HlN4yIPfiOrpDnZ3XqYCkrTbIER3Fu+PDIBQdPMjlVbaatnWurff54Fmi7nVkGrDb5rgSo3y1+6WkD

$ python3 -m aptsign sign-file subkey.sec < Test
Key: value
OtherKey: otherValue
Signatures:
 s1yq1YBOsjZYT9+8Hq2y2MBlvJ31EDjFf8OzgufpeFVDuO3aZMWDJJvU9SP5KHW+G+Uo25myfQRjQdojh8KqDf//////////AAAAABt1KuFrxNctO84dWeXgwiQutPJjUs2mA8uNqBcNGC7yPJs2x/ZVZ6DFIRRfzq1iq8EwzKA/sW/nIBSc3ri71QyUksnq7Rz74vEe9eFD+ZbuuD/audMDeqOebiJI8459yEHbZr/mI2rRKV1Voib0OQf3uj8ZlDCT9lydzk2dgtMC

$ python3 -m aptsign sign-file subkey.sec < Test | python3 -m aptsign verify-file
I: Verified signature from s1yq1YBOsjZYT9+8Hq2y2MBlvJ31EDjFf8OzgufpeFU=:Q7jt2mTFgySb1PUj+Sh1vhvlKNuZsn0EY0HaI4fCqg0=
Key: value
OtherKey: otherValue
$ python3 -m aptsign public-key subkey.sec
Q7jt2mTFgySb1PUj+Sh1vhvlKNuZsn0EY0HaI4fCqg0=
$ python3 -m aptsign public-key primary.sec
s1yq1YBOsjZYT9+8Hq2y2MBlvJ31EDjFf8OzgufpeFU=
```
